FROM python:3.5.1-alpine

ENV MONGO mongodb
ENV MONGO_PORT 27017

ADD . /app
RUN apk --no-cache --update add  \
    build-base=0.4-r1  && \
    pip install --upgrade pip==20.3.4 && \
    pip --no-cache-dir install -r /app/requirements.txt && \
    apk del build-base

WORKDIR /app/ui
EXPOSE 8000
ENV FLASK_APP=ui.py
CMD [ "gunicorn", "ui:app", "-b", "0.0.0.0" ]
